Bakalářská práce
Řídící jednotka pro vzdálenou správu domácnosti s ESP32
Zdeněk Muzika, 2022

Obsah tohoto repozitáře:
├─ board..............Adresář se schématem a návrhem desky jednotky
├─ extras.............Adresář obsahující použité úpravy knihovny esp32_https_server
├─ project............Adresář se zdrojovou formou programu
│ ├─ include..........Hlavičkové soubory
│ ├─ src..............Zdrojové kódy
│ └─ platformio.ini...Inicializační soubor projektu pro VS Code s PlatformIO
├─ thesis_src.........Adresář se zdrojovou formou práce v LaTeX
│ ├─ thesis.zip.......Komprimovaná složka ZIP s kompletní prací včetně šablony
│ └─ text.............Adresář se zdrojovým kódem textu práce
├─ thesis.pdf.........Text práce ve formátu PDF
└─ readme.txt.........Tento readme textový soubor
