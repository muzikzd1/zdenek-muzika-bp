#include "server.h"
#include "io_ports.h" // Relay control

#include "cert.h"
#include "private_key.h"

using namespace httpsserver;

SSLCert cert = SSLCert(
  example_crt_DER, example_crt_DER_len,
  example_key_DER, example_key_DER_len
);
HTTPSServer secureServer = HTTPSServer(&cert);

Status st;

char access_username[64 + 1];
char access_password[64 + 1];

void handle_root(HTTPRequest * req, HTTPResponse * res) {
  res->setHeader("Content-Type", "text/html; charset=utf-8");

  res->println("<!DOCTYPE html><html>");
  res->println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
  res->println("<link rel=\"icon\" href=\"data:,\">");
  res->println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
  res->println(".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;");
  res->println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
  res->println(".button2 {background-color: #555555;}</style></head>");

  res->println("<body><h1>ESP32 Relay control</h1>");

  for (int id = 0; id < RELAY_CNT; id++) {
    res->print("<p>RELAY ");
    res->print(id + 1);
    res->print(" is ");
    res->print(get_relay_state(id) ? "OFF" : "ON");
    res->println("</p>");

    res->print("<p><form method=\"post\" action=\"/relay/");
    res->print(id);
    res->print("/");
    res->print(get_relay_state(id) ? 1 : 0);
    res->println("\">");

    res->print("<button type=\"submit\" ");
    res->print("class=\"button");
    if (get_relay_state(id)) {
      res->print(" button2\">TURN ON");
    } else {
      res->print("\">TURN OFF");
    }
    res->println("</button>");
    res->println("</form>");
    res->println("</p>");
  }

  res->print("<h2>Temperature: ");
  res->printf("%.2f", st.temperature);
  res->println("°C</h2>");

  res->print("<h2>Humidity: ");
  res->printf("%.0f", st.humidity);
  res->println("%</h2>");

  res->print("<p>Accessed: ");
  res->printf("%02u:%02u:%02u %02u/%02u/%04u", 
                st.date.hour(), 
                st.date.minute(), 
                st.date.second(), 
                st.date.day(),
                st.date.month(),
                st.date.year());
  res->println("</p>");

  res->print("</body></html>");
}

void handle_switch(HTTPRequest * req, HTTPResponse * res) {
  req->discardRequestBody();

  ResourceParameters * params = req->getParams();

  uint32_t idx_r = parseUInt(params->getPathParameter(0));
  relay_set(idx_r, params->getPathParameter(1) == "0" ? HIGH : LOW); // Control is negated for relays.

  res->setStatusCode(303);
  res->setStatusText("See Other"); 
  res->setHeader("Location", "/");
  res->println("Redirecting...");
}

void handle_not_found(HTTPRequest * req, HTTPResponse * res) {
  req->discardRequestBody();
  res->setStatusCode(404);
  res->setStatusText("Not Found");
  res->setHeader("Content-Type", "text/html");
  res->println("<!DOCTYPE html>");
  res->println("<html>");
  res->println("<head><title>Not Found</title></head>");
  res->println("<body><h1>404 Not Found</h1><p>The requested resource was not found.</p></body>");
  res->println("</html>");
}

bool validate_relay_id(std::string s) {
  uint32_t id = parseUInt(s);
  return id < RELAY_CNT && (id != 0 || s == "0");
}

bool validate_relay_state(std::string s) {
  return s == "0" || s == "1";
}

void middleware_access_control(HTTPRequest * req, HTTPResponse * res, std::function<void()> next) {
  std::string reqUsername = req->getBasicAuthUser();
  std::string reqPassword = req->getBasicAuthPassword();

  if (!strcmp(reqUsername.c_str(), access_username) && !strcmp(reqPassword.c_str(), access_password)) {
    next();
  } else {
    res->setStatusCode(401);
    res->setStatusText("Unauthorized");
    res->setHeader("Content-Type", "text/plain");
    res->setHeader("WWW-Authenticate", "Basic realm=\"ESP32 privileged area\"");
    res->println("401. Unauthorized");
  }
}

void server_setup() {
 
  ResourceNode * node_root = new ResourceNode("/", "GET", &handle_root);
  ResourceNode * node_switch = new ResourceNode("/relay/*/*", "POST", &handle_switch);
  ResourceNode * node_not_found = new ResourceNode("", "GET", &handle_not_found);
 
  node_switch->addPathParamValidator(0, &validateUnsignedInteger);
  node_switch->addPathParamValidator(0, &validate_relay_id);
  node_switch->addPathParamValidator(1, &validate_relay_state);

  secureServer.registerNode(node_root);
  secureServer.registerNode(node_switch);
  secureServer.setDefaultNode(node_not_found);

  secureServer.addMiddleware(&middleware_access_control);

  secureServer.start();
}


void server_loop() {
  secureServer.loop();
}

void server_update_status(Status current_st) {
  st = current_st;
}

void access_set(const char * new_usr, const char * new_pwd) {
  memcpy(access_username, new_usr, 64);
  memcpy(access_password, new_pwd, 64);
}
