#include "network.h"

// Definitions for WT32-ETH01 used by ETH.h library
#ifndef ETH_PHY_ADDR
  #define ETH_PHY_ADDR    1
#endif

#ifndef ETH_PHY_TYPE
  #define ETH_PHY_TYPE    ETH_PHY_LAN8720
#endif

#ifndef ETH_PHY_POWER
  #define ETH_PHY_POWER   16
#endif

#ifndef ETH_PHY_MDC
  #define ETH_PHY_MDC     23
#endif

#ifndef ETH_PHY_MDIO
  #define ETH_PHY_MDIO    18
#endif

#ifndef ETH_CLK_MODE
  #define ETH_CLK_MODE    ETH_CLOCK_GPIO0_IN
#endif

#include <ETH.h>

bool eth_connected;

void ethernet_init() {
  eth_connected = false;
  WiFi.onEvent(ethernet_wifi_handler);
  ETH.begin(ETH_PHY_ADDR, ETH_PHY_POWER);
}

void ethernet_wifi_handler(WiFiEvent_t event) {
  switch (event) {
    case SYSTEM_EVENT_ETH_START:
      break;
    case SYSTEM_EVENT_ETH_CONNECTED:
      break;
    case SYSTEM_EVENT_ETH_GOT_IP:
      eth_connected = true;
      break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:
      eth_connected = false;
      break;
    case SYSTEM_EVENT_ETH_STOP:
      eth_connected = false;
      break;
    default:
      break;
  }
}

bool ethernet_connected() {
  return eth_connected;
}

IPAddress get_ethernet_ip() {
  return ETH.localIP();
} 

void wifi_init(const char * ssid, const char * pass) {
  WiFi.disconnect();
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
}

bool wifi_connected() {
  return WiFi.isConnected();
}

void wifi_connect_to(const char * ssid, const char * pass) {
  WiFi.disconnect();
  WiFi.begin(ssid, pass);
}

IPAddress get_wifi_ip() {
  return WiFi.localIP();
} 
