#include "rtc.h"

RTC_PCF8563 rtc;

void rtc_init() {
    while (!rtc.begin()) {
        delay(500);
    }
    if (rtc.lostPower()) {
        rtc.adjust(DateTime());
    }
    rtc.start();
}

DateTime get_datetime() {
    return rtc.now();
}

void set_datetime(DateTime setting) {
    rtc.adjust(setting);
}