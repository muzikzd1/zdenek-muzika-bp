#include "sensor.h"

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

#define DHTPIN  4
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

void dht_init() {
    dht.begin();
}

bool dht_reading(float &out_temperature, float &out_humidity) {
    float temperature = dht.readTemperature();
    float humidity = dht.readHumidity();
    if (isnan(temperature) || isnan(humidity)) {
        return false;
    }
    out_temperature = temperature;
    out_humidity = humidity;
    return true;
}
