#include "user_interface.h"
#include "io_ports.h"
#include "screen.h"
#include "memory.h" // For max length of ssid, pass etc., that can be saved.

#define ST_MAIN_MENU        0
#define ST_STATUS           1
#define ST_SSID             2
#define ST_WIFI_PWD         3
#define ST_ACC_USR          4
#define ST_ACC_PWD          5
#define ST_SET_CONFIRM      6
#define ST_SET_RTC          7
#define ST_CONFIRM_SET_RTC  8

int ui_state;

#define MAIN_MENU_CNT  11
const char  *  items[] = {  "View status",
                            "Set SSID",
                            "Set Wifi Password",
                            "Set Access Username",
                            "Set Access Password",
                            "Confirm settings",
                            "Set RTC",
                            "Switch 1",
                            "Switch 2",
                            "Switch 3",
                            "Switch 4"};

// screen variables for ui loop to work with
Menu                main_menu(items, MAIN_MENU_CNT);
StatusBoard         status_board;
StringInput         string_input_screen;
Confirmation        confirm_screen;
TimeSet             time_set_screen;

// data status for ui to work with at status board etc...
Status              data_status;

void ui_init() {
    io_init();
    screen_init();
    ui_state = ST_MAIN_MENU;
}

void ui_update_status(Status new_data_status) {
    data_status = new_data_status;
}


int ui_loop(DateTime &date_out, char * output_buffer, int &output_len) {
    unsigned char input = buttons_loop(); // read input from buttons
    int ret = 0; // for return value of inner functions
    int output_state = UI_OUT_DEFAULT; // for return value of ui_loop()
    switch (ui_state) {
        case ST_MAIN_MENU :
            ret = main_menu.control(input);
            switch (ret) {
                case 0 :
                    swap_screen();
                    status_board.update(data_status);
                    ui_state = ST_STATUS;
                    break;
                case 1 :
                    swap_screen();                    
                    string_input_screen = StringInput("ENTER SSID:", MEM_MAX_STRING);
                    ui_state = ST_SSID;
                    break;
                case 2 :
                    swap_screen();
                    string_input_screen = StringInput("ENTER WIFI PWD:", MEM_MAX_STRING);
                    ui_state = ST_WIFI_PWD;
                    break;
                case 3 :
                    swap_screen();
                    string_input_screen = StringInput("ENTER ACC USR:", MEM_MAX_STRING);
                    ui_state = ST_ACC_USR;
                    break;
                case 4 :
                    swap_screen();
                    string_input_screen = StringInput("ENTER ACC PWD:", MEM_MAX_STRING);
                    ui_state = ST_ACC_PWD;
                    break;
                case 5 :
                    swap_screen();
                    confirm_screen = Confirmation("Save new settings?");
                    ui_state = ST_SET_CONFIRM;
                    break;
                case 6 :
                    swap_screen();
                    time_set_screen = TimeSet(data_status.date);
                    ui_state = ST_SET_RTC;
                    break;
                case 7 :
                    swap_screen();
                    relay_set(0, !get_relay_state(0));
                    break;
                case 8 :
                    swap_screen();
                    relay_set(1, !get_relay_state(1));
                    break;
                case 9 :
                    swap_screen();
                    relay_set(2, !get_relay_state(2));
                    break;
                case 10 :
                    swap_screen();
                    relay_set(3, !get_relay_state(3));
                    break;
                default:
                    break;
            }
            break;
        case ST_STATUS:
            status_board.update(data_status);
            ret = status_board.control(input);
            if (ret == -2) {
                swap_screen();
                ui_state = ST_MAIN_MENU;
            }
            break;
        case ST_SSID:
            ret = string_input_screen.control(input);
            switch (ret) {
                case 0:
                    output_len = string_input_screen.copy_text(output_buffer);
                    output_state = UI_OUT_WIFI_SSID;
                case -2:
                    swap_screen();
                    ui_state = ST_MAIN_MENU;
                    break;
                default:
                    break;
            }
            break;
        case ST_WIFI_PWD:
            ret = string_input_screen.control(input);
            switch (ret) {
                case 0:
                    output_len = string_input_screen.copy_text(output_buffer);
                    output_state = UI_OUT_WIFI_PWD;
                case -2:
                    swap_screen();
                    ui_state = ST_MAIN_MENU;
                    break;
                default:
                    break;
            }
            break;
        case ST_ACC_USR:
            ret = string_input_screen.control(input);
            switch (ret) {
                case 0:
                    output_len = string_input_screen.copy_text(output_buffer);
                    output_state = UI_OUT_ACC_USR;
                case -2:
                    swap_screen();
                    ui_state = ST_MAIN_MENU;
                    break;
                default:
                    break;
            }
        case ST_ACC_PWD:
            ret = string_input_screen.control(input);
            switch (ret) {
                case 0:
                    output_len = string_input_screen.copy_text(output_buffer);
                    output_state = UI_OUT_ACC_PWD;
                case -2:
                    swap_screen();
                    ui_state = ST_MAIN_MENU;
                    break;
                default:
                    break;
            }
            break;
        case ST_SET_CONFIRM:
            ret = confirm_screen.control(input);
            switch (ret) {
                case 0:
                    output_state = UI_OUT_SET_CONFIRM;
                case -2:
                    swap_screen();
                    ui_state = ST_MAIN_MENU;
                    break;
                default:
                    break;
            }
            break;
        case ST_SET_RTC:
            ret = time_set_screen.control(input);
            switch (ret) {
                case 0:
                    date_out = time_set_screen.get_datetime();
                    swap_screen();
                    confirm_screen = Confirmation("Set new date-time?");
                    ui_state = ST_CONFIRM_SET_RTC;
                    break;
                default:
                    break;
            }
            break;
        case ST_CONFIRM_SET_RTC:
            ret = confirm_screen.control(input);
            switch (ret) {
                case 0:
                    output_state = UI_OUT_SET_RTC;
                case -2:
                    swap_screen();
                    ui_state = ST_MAIN_MENU;
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }

    switch (ui_state) {
        case ST_MAIN_MENU :
            main_menu.draw();
            break;
        case ST_STATUS :
            status_board.draw();
            break;
        case ST_SSID:
        case ST_WIFI_PWD:
        case ST_ACC_USR:
        case ST_ACC_PWD:
            string_input_screen.draw();
            break;
        case ST_SET_CONFIRM:
        case ST_CONFIRM_SET_RTC:
            confirm_screen.draw();
            break;
        case ST_SET_RTC:
            time_set_screen.draw();
            break;
        default:
            break;
    }
    return output_state;
}