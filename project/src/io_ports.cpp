#include "io_ports.h"
#include <Adafruit_MCP23X17.h>

// Button pins and helper declarations.
#define BUTTON_CNT          5
const int button_gpio[]     = {8, 9, 10, 11, 12};
const int button_mask[]     = { BUTTON_RIGHT, 
                                BUTTON_DOWN,
                                BUTTON_MIDDLE,
                                BUTTON_LEFT,
                                BUTTON_UP};

// Debounce time in ms.
#define BUTTON_DBC_DELAY    20

// Debounce helper variables.
unsigned long   btn_debounce_t[BUTTON_CNT];
int             btn_previous_state[BUTTON_CNT];

// Relay declarations.      
const int relay_gpio[RELAY_CNT] = {0, 1, 2, 3};
int relay_state[RELAY_CNT];

// Instance of mcp port expander class.
Adafruit_MCP23X17 mcp;

void io_init() {
    while (!mcp.begin_I2C()) {
        delay(500);
    }
    button_init();
    relay_init();
}

void button_init() {
    for (int i = 0; i < BUTTON_CNT; i++) {
        mcp.pinMode(button_gpio[i], INPUT_PULLUP);
        btn_debounce_t[i] = 0;
        btn_previous_state[i] = HIGH;
    }
}

void relay_init() {
    for (int i = 0; i < RELAY_CNT; i++) {
        mcp.pinMode(relay_gpio[i], OUTPUT);
        relay_state[i] = HIGH;
        mcp.digitalWrite(relay_gpio[i], relay_state[i]); 
    }
}

unsigned char buttons_loop() {
    unsigned char ret_val = 0;
    int btn_current_state[BUTTON_CNT];

    for (int i = 0; i < BUTTON_CNT; i++) {
        btn_current_state[i] = mcp.digitalRead(button_gpio[i]);

        if (btn_current_state[i] != btn_previous_state[i]) {
            btn_debounce_t[i] = millis();
        }

        if ((millis() - btn_debounce_t[i]) > BUTTON_DBC_DELAY) {
            if (btn_current_state[i] == LOW) {
                ret_val |= button_mask[i];
            }
        }

        btn_previous_state[i] = btn_current_state[i];
    }
    return ret_val;
}

void relay_set(int idx_r, int value) {
    relay_state[idx_r] = value;
    mcp.digitalWrite(relay_gpio[idx_r], relay_state[idx_r]);
}

int get_relay_state(int idx_r) {
    return relay_state[idx_r];
}
