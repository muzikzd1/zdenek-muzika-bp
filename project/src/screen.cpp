#include "screen.h"
#include "io_ports.h"
#include <U8g2lib.h>
#include "RTClib.h" // for datetime functions

// U8G2 constructor for SH1106; draw graphics via this instance
U8G2_SH1106_128X64_NONAME_F_HW_I2C  u8g2(U8G2_R0, U8X8_PIN_NONE);

#define BASIC_FONT          u8g2_font_6x12_t_symbols
#define BASIC_FONT_HEIGHT   12
#define BASIC_FONT_WIDTH    6

#define MICRO_FONT          u8g2_font_micro_mr
#define MICRO_FONT_HEIGHT   5
#define MICRO_FONT_WIDTH    4

#define REPEAT_DELAY        250
unsigned long               repeat_t;
bool                        hold;
bool                        swap;

void screen_init() {
    u8g2.begin();
    repeat_t    = 0;
    hold        = false;
    swap        = false;
}

void swap_screen() {
    swap = true;
}

//===========================================================

void Screen::draw() {
    u8g2.drawStr(8, 16, "Invalid screen");
}

int Screen::control(unsigned char input) {
    unsigned long current_t = millis();
    if (input == 0) { // all buttons released
        hold = false;
        swap = false; // to ensure only to repeat button hold, if screen just didnt swap
        return -1;
    } 
    if (current_t - repeat_t > REPEAT_DELAY) {  // check if repeat on button hold
        hold = false;
    }
    if (hold || swap) {
        return -1;
    }
    hold = true;
    repeat_t = current_t;

    // inputs need to be exact!
    // multiple inputs at the same time are ignored
    if (input == BUTTON_MIDDLE)
        return middle_press();
    if (input == BUTTON_RIGHT)
        return right_press();
    if (input == BUTTON_LEFT)
        return left_press();
    if (input == BUTTON_UP)
        return up_press();
    if (input == BUTTON_DOWN)
        return down_press();
    return -1;
}

int Screen::middle_press() {
    return -1;
}

int Screen::right_press() {
    return -1;
}

int Screen::left_press() {
    return -1;
}

int Screen::up_press() {
    return -1;
}

int Screen::down_press() {
    return -1;
}

//===========================================================

Menu::Menu(const char * items[], int items_len) {
    select = select_offset = 0;
    this->items = items;
    this->items_len = items_len;
}

#define MENU_MAX_ROWS           5
#define MENU_LEFT_OFFSET        10
#define MENU_CURSOR_CORRECTION  2

void Menu::draw() {

    u8g2.clearBuffer();
    u8g2.setFont(BASIC_FONT);
    int y;
    // menu items drawing.
    for (int i = 0; i < MENU_MAX_ROWS && i < items_len; i++) {
        y = (i + 1) * BASIC_FONT_HEIGHT + i; // 1px between rows
        u8g2.drawStr(MENU_LEFT_OFFSET, y , items[select_offset + i]);
    }
    // cursor drawing
    // which row to put cursor on.
    y = select * BASIC_FONT_HEIGHT + select;
    y += BASIC_FONT_HEIGHT / 2; // center to font height.
    y += MENU_CURSOR_CORRECTION; // correction (to better center the cursor for most symbols/characters)
    u8g2.drawCircle(5, y, 1);
    u8g2.sendBuffer();
}

int Menu::middle_press() {
    return select + select_offset;
}

int Menu::up_press() {
    if (select > 0) { // check if not on upper edge of menu
        select--;
    } else {
        if (select_offset > 0) { // check if can scroll up
            select_offset--;
        }
    }
    return -1;
}

int Menu::down_press() {
    if (select < MENU_MAX_ROWS - 1) { // check if not on bottom edge of menu
        if (select < items_len - 1) { // edge case for menus shorter than max rows
            select++;
        }
    } else {
        if (select_offset < items_len - MENU_MAX_ROWS) { // check if can scroll down
            select_offset++;
        }
    }
    return -1;
}

//===========================================================

StatusBoard::StatusBoard() {
    current_st.date = DateTime((uint32_t) 0);
    current_st.humidity = -1000;
    current_st.temperature = -1000;
}

#define STATUS_BUFFER_LEN   40

void StatusBoard::draw() {
    u8g2.clearBuffer();
    char buffer[STATUS_BUFFER_LEN];
    u8g2.setFont(BASIC_FONT);
    if (current_st.temperature > -999) {
        snprintf(buffer, STATUS_BUFFER_LEN, "Temp.: %.2f°C", current_st.temperature);
    } else {
        snprintf(buffer, STATUS_BUFFER_LEN, "Temp.: unknown");
    }
    u8g2.drawUTF8(0, BASIC_FONT_HEIGHT, buffer);
    if (current_st.humidity > -999) {
        snprintf(buffer, STATUS_BUFFER_LEN, "Humidity: %.0f%%", current_st.humidity);
    } else {
        snprintf(buffer, STATUS_BUFFER_LEN, "Humidity: unknown");
    }
    u8g2.drawUTF8(0, 2 * BASIC_FONT_HEIGHT + 1, buffer);
    snprintf(buffer, STATUS_BUFFER_LEN, "%02u:%02u:%02u %02u/%02u/%04u", 
                current_st.date.hour(), 
                current_st.date.minute(), 
                current_st.date.second(), 
                current_st.date.day(),
                current_st.date.month(),
                current_st.date.year());
    u8g2.drawUTF8(0, 3 * BASIC_FONT_HEIGHT + 2, buffer);
    u8g2.drawStr(0, 4 * BASIC_FONT_HEIGHT + 3, "Eth: ");
    u8g2.drawStr(6 * BASIC_FONT_WIDTH, 4 * BASIC_FONT_HEIGHT + 3, current_st.ethernet_ip.toString().c_str());
    u8g2.drawStr(0, 5 * BASIC_FONT_HEIGHT + 4, "WiFi: ");
    u8g2.drawStr(6 * BASIC_FONT_WIDTH, 5 * BASIC_FONT_HEIGHT + 4, current_st.wifi_ip.toString().c_str());

    u8g2.sendBuffer();
}

void StatusBoard::update(Status st) {
    current_st = st;
}

int StatusBoard::middle_press() {
    return -2;
}

//===========================================================
StringInput::StringInput() {
    this->dialogue = "Default";
    select_x = select_y = select_offset = text_len = 0;
    text[0] = '\0';
}

StringInput::StringInput(const char * dialogue, int max_input) {
    this->dialogue = dialogue;
    select_x = select_y = select_offset = text_len = 0;
    text[0] = '\0';
    if (max_input < 0 || max_input > MAX_STRING_INPUT) {
        this->max_input = MAX_STRING_INPUT;
    } else {
        this->max_input = max_input;
    }
}

// NOTE: Marked values may cause the dialogue and text to theoreticaly overlap, however none of the ASCII symbols should have enough height for it to happen
// done so all the stuff could be viewable on one screen
#define SI_DIALOGUE_Y       5   //<===
#define SI_FIRST_ROW_Y      16  //<===
#define SI_ROW_OFFSET       15

#define SI_LEFT_OFFSET      7
#define SI_MAX_ROW_SIZE     13
#define SI_MAX_PREVIEW_SIZE 18
#define SI_CHARSET_ROW_SIZE 47

const char *charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+*-/\\<=>|~^abcdefghijklmnopqrstuvwxyz_.,:;?!'\"`#$%&()[]{}@";

void StringInput::draw() {
    u8g2.clearBuffer();
    int x, y;
    int selector_len = BASIC_FONT_WIDTH;
    // draw dialogue
    u8g2.setFont(MICRO_FONT);
    x = SI_LEFT_OFFSET;
    y = SI_DIALOGUE_Y;
    u8g2.drawStr(x, y, dialogue);
    // draw preview of string that is being written.
    u8g2.setFont(BASIC_FONT);
    x = SI_LEFT_OFFSET;
    y = SI_FIRST_ROW_Y;
    // check if preview can fit into area, if not display only the last chars
    int text_offset = 0;
    if (text_len > SI_MAX_PREVIEW_SIZE) {
        text_offset = text_len - SI_MAX_PREVIEW_SIZE;
    }
    u8g2.drawStr(x, y, text + text_offset);

    // draw column of character selection.
    for (int i = 0; i < SI_MAX_ROW_SIZE; i++) {
        x = SI_LEFT_OFFSET + i * (BASIC_FONT_WIDTH + 3);
        y = SI_FIRST_ROW_Y + SI_ROW_OFFSET;
        u8g2.setCursor(x, y);
        u8g2.print(charset[i + select_offset]);
        y += SI_ROW_OFFSET;
        u8g2.setCursor(x, y);
        u8g2.print(charset[i + select_offset + SI_CHARSET_ROW_SIZE]);
    }

    // draw controls.
    x = SI_LEFT_OFFSET + 4 * (BASIC_FONT_WIDTH + 3);
    y = SI_FIRST_ROW_Y + 3 * SI_ROW_OFFSET;
    u8g2.drawUTF8(x, y,"✓");
    x += BASIC_FONT_WIDTH + 3;
    u8g2.drawUTF8(x, y,"✗");
    x += BASIC_FONT_WIDTH + 3;
    u8g2.drawUTF8(x, y, "↩");
    x += BASIC_FONT_WIDTH + 3;
    u8g2.drawStr(x, y, "SPACE");

    // draw selector.
    if (select_y < 2) {
        x = SI_LEFT_OFFSET + select_x * (BASIC_FONT_WIDTH + 3);
        y = SI_FIRST_ROW_Y + (select_y + 1) * SI_ROW_OFFSET + 2;
    } else { // control selector.
        x = SI_LEFT_OFFSET + (4 + select_x) * (BASIC_FONT_WIDTH + 3);
        y = SI_FIRST_ROW_Y + 3 * SI_ROW_OFFSET + 2;
        if (select_x == 3) { // SPACE selector.
            selector_len *= 5;
        }
    }
    u8g2.drawLine(x, y, x + selector_len - 1, y);
    u8g2.sendBuffer();
}

int StringInput::copy_text(char * buffer) {
    memcpy(buffer, text, text_len + 1); // Copy string with zero termination aswell
    return text_len;
}

int StringInput::middle_press() {
    if (select_y == 2) { // control panel actions
        switch (select_x) {
            case 0: // confirmation
                return 0;
                break;
            case 1: // cancel
                return -2;
                break;
            case 2: // backspace
                if (text_len > 0) {
                    text_len--;
                    text[text_len] = '\0';
                }
                break;
            case 3: // space
                if (text_len < max_input) {
                    text[text_len] = ' ';
                    text_len++;
                    text[text_len] = '\0';
                }
                break;
            default:
                break;
        }
    } else { // append selected character
        if (text_len < max_input) {
            text[text_len] = charset[select_y * SI_CHARSET_ROW_SIZE + select_x + select_offset];
            text_len++;
            text[text_len] = '\0';
        }
    }
    return -1;
}

int StringInput::up_press() {
    if (select_y > 0) { // check if not on edge
        if (select_y == 2) { // moving from control panel row
            select_x += 4;
        }
        select_y--;
    }
    return -1;
}

int StringInput::down_press() {
    if (select_y < 2) { // check if not on edge
        if (select_y == 1) { // moving from control panel row
            if (select_x <= 4) {  
                select_x = 0;
            } else if (select_x >= 7) {
                select_x = 3;
            } else {
                select_x -= 4;
            }
        }
        select_y++;
    }
    return -1;
}

int StringInput::right_press() {
    if (select_y == 2) { // on control panel row
        if (select_x < 3) { // check if not on edge of control panel
            select_x++;
        }
    } else {
        if (select_x < SI_MAX_ROW_SIZE - 1) { // check if not on edge
            select_x++;
        } else if (select_offset < SI_CHARSET_ROW_SIZE - SI_MAX_ROW_SIZE) { // check if can scroll/offset right
            select_offset++;
        } 
    }
    return -1;
}

int StringInput::left_press() {
    if (select_y == 2) { // on control panel row
        if (select_x > 0) { // check if not on edge of control panel
            select_x--;
        }
    } else {
        if (select_x > 0) { // check if not on edge
            select_x--;
        } else if (select_offset > 0) { // check if can scroll/offset left
            select_offset--;
        } 
    }
    return -1;
}

//===========================================================

Confirmation::Confirmation() {
    this->dialogue = "Default";
    confirm = false;
}

Confirmation::Confirmation(const char * dialogue) {
    this->dialogue = dialogue;
    confirm = false;
}

void Confirmation::draw() {
    u8g2.clearBuffer();
    u8g2.setFont(BASIC_FONT);
    u8g2.drawStr(0, BASIC_FONT_HEIGHT, dialogue);
    u8g2.drawUTF8(52, 35, "✓");
    u8g2.drawUTF8(71, 35, "✗");
    if (confirm) {
        u8g2.drawLine(52, 37, 57, 37);
    } else {
        u8g2.drawLine(71, 37, 76, 37);
    }
    u8g2.sendBuffer();
}

int Confirmation::middle_press() {
    if (confirm)
        return 0;
    return -2;
}

int Confirmation::left_press() {
    confirm = true;
    return -1;
}

int Confirmation::right_press() {
    confirm = false;
    return -1;
}

//===========================================================

TimeSet::TimeSet() {
    hours = 12;
    minutes = 0;
    seconds = 0;
    day = 1;
    month = 1;
    year = 2000;
    offset = 0;
}

TimeSet::TimeSet(DateTime initial) {
    hours = initial.hour();
    minutes = initial.minute();
    seconds = initial.second();
    day = initial.day();
    month = initial.month();
    year = initial.year();
    offset = 0;
}

#define TIME_SET_LEFT_OFFSET    7
#define TIME_SET_TOP_OFFSET     37

void TimeSet::draw() {
    u8g2.clearBuffer();
    u8g2.setFont(BASIC_FONT);

    u8g2.setCursor(TIME_SET_LEFT_OFFSET, TIME_SET_TOP_OFFSET);  
    u8g2.printf("%02u:%02u:%02u %02u/%02u/%04u", 
                hours, 
                minutes, 
                seconds,
                day,
                month,
                year);
    int x = TIME_SET_LEFT_OFFSET + 3;
    if (offset < 5) { // non-year setting 
        x += offset * (3 * BASIC_FONT_WIDTH);
    } else { // year setting - slightly different offset due to year being 4 digit
        x += 16 * BASIC_FONT_WIDTH;
    }
    u8g2.drawUTF8(x, TIME_SET_TOP_OFFSET - BASIC_FONT_HEIGHT - 1, "↑");
    u8g2.drawUTF8(x, TIME_SET_TOP_OFFSET + BASIC_FONT_HEIGHT + 1, "↓");
    u8g2.sendBuffer();
}

DateTime TimeSet::get_datetime() {
    return DateTime(year, month, day, hours, minutes, seconds);
}

uint8_t TimeSet::max_day(uint8_t month, uint16_t year) {
    switch (month) {
        case 1 : 
        case 3 :
        case 5 :
        case 7 :
        case 8 :
        case 10 :
        case 12 :
            return 31;
        case 2 :
            if (!(year % 400) || (year % 100 && !(year % 4))) {
                return 29;
            } else {
                return 28;
            }
        default:
            return 30;
    }
}

int TimeSet::middle_press() {
    return 0;
}

int TimeSet::up_press() {
    uint8_t day_upper;
    switch (offset) {
        case 0:
            hours = hours < 23 ? hours + 1 : 0;
            break;
        case 1:
            minutes = minutes < 59 ? minutes + 1 : 0;
            break;
        case 2:
            seconds = seconds < 59 ? seconds + 1 : 0;
            break;
        case 3:
            day_upper =  max_day(month, year);
            day = day < day_upper ? day + 1 : 1;
            break;
        case 4:
            month = month < 12 ? month + 1 : 1;
            day_upper = max_day(month, year);
            if (day > day_upper)
                day = day_upper;
            break;
        case 5:
            year = year < 2099 ? year + 1 : 2000;
            day_upper = max_day(month, year);
            if (day > day_upper)
                day = day_upper;
            break;
        default:
            break;
    }
    return -1;
}

int TimeSet::down_press() {
    uint8_t day_upper;
    switch (offset) {
        case 0:
            hours = hours > 0 ? hours - 1 : 23;
            break;
        case 1:
            minutes = minutes > 0 ? minutes - 1 : 59;
            break;
        case 2:
            seconds = seconds > 0 ? seconds - 1 : 59;
            break;
        case 3:;
            day = day > 1 ? day - 1 : max_day(month, year);
            break;
        case 4:
            month = month > 1 ? month - 1 : 12;
            day_upper = max_day(month, year);
            if (day > day_upper)
                day = day_upper;
            break;
        case 5:
            year = year > 2000 ? year - 1 : 2099;
            day_upper = max_day(month, year);
            if (day > day_upper)
                day = day_upper;
            break;
        default:
            break;
    }
    return -1;
}

int TimeSet::left_press() {
    if (offset > 0) {
        offset--;
    }
    return -1;
}

int TimeSet::right_press() {
    if (offset < 5) {
        offset++;
    }
    return -1;
}
