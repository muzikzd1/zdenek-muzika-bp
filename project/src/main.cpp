#include <Arduino.h>
#include <Wire.h>

#include "user_interface.h"
#include "sensor.h"
#include "rtc.h"
#include "status.h"
#include "network.h"
#include "server.h"
#include "memory.h"

unsigned long previous_time_ui;
char          tmp_data[MEM_MAX_STRING + 1];
int           tmp_data_len;
DateTime      tmp_date;

unsigned long previous_time_dht;
unsigned long previous_time_rtc;
bool          data_update;
Status        current_st;

char wifi_ssid[MEM_MAX_STRING + 1];
char wifi_pass[MEM_MAX_STRING + 1];
bool wifi_dirty;
bool access_dirty;

char tmp_access_username[MEM_MAX_STRING + 1];
char tmp_access_password[MEM_MAX_STRING + 1];

unsigned long previous_time_server;
unsigned long previous_time_ip;

void setup(void) {
  Wire.begin(14, 15);
  rtc_init();
  dht_init();
  ui_init();
  memory_init();
  ethernet_init();

  memread_wifi_ssid(wifi_ssid);
  memread_wifi_pass(wifi_pass);
  wifi_ssid[MEM_MAX_STRING] = '\0';
  wifi_pass[MEM_MAX_STRING] = '\0';
  wifi_dirty = false;

  memread_acc_usr(tmp_access_username);
  memread_acc_pwd(tmp_access_password);
  tmp_access_username[MEM_MAX_STRING] = '\0';
  tmp_access_password[MEM_MAX_STRING] = '\0';
  access_set(tmp_access_username, tmp_access_password);
  access_dirty = false;

  wifi_init(wifi_ssid, wifi_pass);
  server_setup();

  previous_time_ui = 0;
  previous_time_dht = 0;
  previous_time_rtc = 0;
  previous_time_server = 0;
  previous_time_ip = 0;

  dht_reading(current_st.temperature, current_st.humidity);
  current_st.date = get_datetime();
  current_st.ethernet_ip = get_ethernet_ip();
  current_st.wifi_ip = get_wifi_ip();
  ui_update_status(current_st);
}

void loop(void) {
  data_update = false;
  unsigned long current_time = millis();
  int ret;

  if ((current_time - previous_time_ui) > 30) {
    ret = ui_loop(tmp_date, tmp_data, tmp_data_len);
    switch(ret) {
      case UI_OUT_WIFI_SSID:
        strcpy(wifi_ssid, tmp_data);
        memwrite_wifi_ssid(wifi_ssid);
        wifi_dirty = true;
        break;
      case UI_OUT_WIFI_PWD:
        strcpy(wifi_pass, tmp_data);
        memwrite_wifi_pass(wifi_pass);
        wifi_dirty = true;
        break;
      case UI_OUT_ACC_USR:
        strcpy(tmp_access_username, tmp_data);
        memwrite_acc_usr(tmp_access_username);
        access_dirty = true;
        break;
      case UI_OUT_ACC_PWD:
        strcpy(tmp_access_password, tmp_data);
        memwrite_acc_pwd(tmp_access_password);
        access_dirty = true;
        break;
      case UI_OUT_SET_CONFIRM:
        if (wifi_dirty || access_dirty) {
          memcommit();
        }
        if (wifi_dirty) {
          wifi_connect_to(wifi_ssid, wifi_pass);
          wifi_dirty = false;
        }
        if (access_dirty) { 
          access_set(tmp_access_username, tmp_access_password);
          access_dirty = false;
        }
        break;
      case UI_OUT_SET_RTC:
        set_datetime(tmp_date);    
        break;
      default:
        break;
    }
    previous_time_ui = current_time;
  }

  if ((current_time - previous_time_dht) > 10000) {
    dht_reading(current_st.temperature, current_st.humidity);
    previous_time_dht = current_time;
    data_update = true;
  }

  if ((current_time - previous_time_rtc) > 100) {
    current_st.date = get_datetime();
    previous_time_rtc = current_time;
    data_update = true;
  }

   if ((current_time - previous_time_server) > 4) {
    server_loop();
    previous_time_server = current_time;
  }

  if ((current_time - previous_time_ip) > 10000) {
    current_st.ethernet_ip = get_ethernet_ip();
    current_st.wifi_ip = get_wifi_ip();
    previous_time_ip = current_time;
    data_update = true;
  }

  if (data_update)
    ui_update_status(current_st);
    server_update_status(current_st);
}