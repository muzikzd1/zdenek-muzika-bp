#include "memory.h"
#include <EEPROM.h>

#define MEM_SIZE        4 * MEM_MAX_STRING

#define WIFI_SSID_ADD   0 * MEM_MAX_STRING
#define WIFI_PASS_ADD   1 * MEM_MAX_STRING
#define ACC_USR_ADD     2 * MEM_MAX_STRING
#define ACC_PWD_ADD     3 * MEM_MAX_STRING

void memory_init() {
    EEPROM.begin(MEM_SIZE);
}

void memread_wifi_ssid(char * destination_ssid) {
    EEPROM.readBytes(WIFI_SSID_ADD, destination_ssid, MEM_MAX_STRING);
}

void memread_wifi_pass(char * destination_pass) {
    EEPROM.readBytes(WIFI_PASS_ADD, destination_pass, MEM_MAX_STRING);
}

void memread_acc_usr(char * destination_usr) {
    EEPROM.readBytes(ACC_USR_ADD, destination_usr, MEM_MAX_STRING);
}

void memread_acc_pwd(char * destination_acc_pass){
    EEPROM.readBytes(ACC_PWD_ADD, destination_acc_pass, MEM_MAX_STRING);
}

void memwrite_wifi_ssid(const char * source_ssid) {
    EEPROM.writeBytes(WIFI_SSID_ADD, source_ssid, MEM_MAX_STRING);
}

void memwrite_wifi_pass(const char * source_pass) {
    EEPROM.writeBytes(WIFI_PASS_ADD, source_pass, MEM_MAX_STRING);
}

void memwrite_acc_usr(const char * source_usr) {
    EEPROM.writeBytes(ACC_USR_ADD, source_usr, MEM_MAX_STRING);
}

void memwrite_acc_pwd(const char * source_acc_pass){
    EEPROM.writeBytes(ACC_PWD_ADD, source_acc_pass, MEM_MAX_STRING);
}

void memcommit() {
    EEPROM.commit();
}