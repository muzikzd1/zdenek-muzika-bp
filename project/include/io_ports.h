#pragma once

// Button bit masks.
#define BUTTON_RIGHT        0x1
#define BUTTON_DOWN         0x2
#define BUTTON_MIDDLE       0x4
#define BUTTON_LEFT         0x8
#define BUTTON_UP           0x10

// Relay count. Also used when managing server interface.
#define RELAY_CNT           4

// MCP23017 and IO initialization function.
// NOTE: Use Wire/I2C begin function before calling this.
void            io_init();

// Buttons initialization function.
// NOTE: First io_init() needs to be called.
void            button_init();

// Relay initialization function.
// NOTE: First io_init() needs to be called.
void            relay_init();

// Reads debounced button states.
// Returns byte with bits set according to button mask.
unsigned char   buttons_loop();

// Sets relay of index idx_r to logic level value HIGH or LOW.
// NOTE: Relay is triggered on LOW.
void            relay_set(int idx_r, int value);

// Gets state of relay indexed idx_r.
int             get_relay_state(int idx_r);