#pragma once

// IMPORTANT:   Saved strings are up to 64 characters, not accounting for possible terminating zero character as 65th character
//              All reads, writes are exactly 64 characters. I/O buffers should account for this, as memory is unintialized on first run.
//              I/O buffers should be 65 characters long and the 65th character should be set to zero.
#define MEM_MAX_STRING  64

// Initializes EEPROM.h non-volatile memory library.
// NOTE: Despite the library name, ESP32 uses flash memory for this purpose.
void    memory_init();

// Reads WiFi ssid from memory.
void    memread_wifi_ssid(char * destination_ssid);

// Reads WiFi password from memory.
void    memread_wifi_pass(char * destination_pass);

// Reads access username from memory.
void     memread_acc_usr(char * destination_usr);

// Reads access password from memory.
void    memread_acc_pwd(char * destination_acc_pass);

// Writes provided WiFi ssid into memory. 
// Requires call to memcommit() to succesfully complete write.
void    memwrite_wifi_ssid(const char * source_ssid);

// Writes provided WiFi password into memory. 
// Requires call to memcommit() to succesfully complete write.
void    memwrite_wifi_pass(const char * source_pass);

// Writes provided access username into memory. 
// Requires call to memcommit() to succesfully complete write.
void    memwrite_acc_usr(const char * source_usr);

// Writes provided access password into memory. 
// Requires call to memcommit() to succesfully complete write.
void    memwrite_acc_pwd(const char * source_acc_pass);

// Commits memory writes.
void    memcommit();

