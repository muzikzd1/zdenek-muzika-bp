#pragma once
#include <WiFi.h>

// Function to initialize ETH.h library and setup ethernet_wifi_handler for ETH.h events.
void        ethernet_init();

// Handler function for WiFi events regarding ethernet connection.
void        ethernet_wifi_handler(WiFiEvent_t event);

// Returns true if ethernet is succefully connected, false otherwise.
bool        ethernet_connected();

// Returns IP address of ethernet interface.
IPAddress   get_ethernet_ip();

// Function to initialize WiFi connection.
void        wifi_init(const char * ssid, const char * pass);

// Returns true if WiFi is succefully connected, false otherwise.
bool        wifi_connected();

// Sets WiFi to connect to a new network.
void        wifi_connect_to(const char * ssid, const char * pass);

// Returns IP address of WiFi interface.
IPAddress   get_wifi_ip();

