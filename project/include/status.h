#pragma once
#include "RTClib.h" // for DateTime

// structure containing data for status board
struct Status {
    float       temperature;
    float       humidity;
    DateTime    date;
    IPAddress   ethernet_ip;
    IPAddress   wifi_ip;
};