#pragma once
 
#include <HTTPSServer.hpp>
#include <SSLCert.hpp>
#include <HTTPRequest.hpp>
#include <HTTPResponse.hpp>
#include <ValidatorFunctions.hpp>

#include "status.h"

// Server setup function, 
void server_setup();

// Server loop function to be called in main loop
void server_loop();

// Updates current Status (from status.h) to server - contains RTC time, temperature, humidity
void server_update_status(Status current_st);

// Sets new access credentials for server
void access_set(const char * new_usr, const char * new_pwd);

// Node to handle requests to root
void handle_root(httpsserver::HTTPRequest * req, httpsserver::HTTPResponse * res);

// Node to handle POST requests to switch relays
void handle_switch(httpsserver::HTTPRequest * req, httpsserver::HTTPResponse * res);

// Node to handle 404
void handle_not_found(httpsserver::HTTPRequest * req, httpsserver::HTTPResponse * res);

// Validation check if relay id is valid.
bool validate_relay_id(std::string s);

// Validation check if state of relay is valid. 
bool validate_relay_state(std::string s);

// Middleware check for access credentials
void middleware_access_control(httpsserver::HTTPRequest * req, httpsserver::HTTPResponse * res, std::function<void()> next);