#pragma once
#include "status.h"

#define MAX_STRING_INPUT    64

// Initalize screen control and u8g2 driver
void screen_init();

// Call whenever changing screens.
// Sets a flag to freeze input until all buttons are released.
// Useful so the user does not accidentaly do unwanted action before having enough time to see the new screen.
void swap_screen();

// Base class for various screens.
class Screen {
    public:
    // Draws default screen.
    virtual void    draw();
    // Control function.
    // Reads input and calls according button press function.
    // Controls repeating when buttons are held.
    // Returns whatever the called function returns, if no button is pressed or input is held frozen at the moment returns -1.
    virtual int     control(unsigned char input);

    protected:
    // Button press functions.
    // Do nothing by default.
    // Return -1 by default.
    virtual int     middle_press();
    virtual int     up_press();
    virtual int     down_press();
    virtual int     left_press();
    virtual int     right_press();
};

// menu screen to display items to choose from
class Menu : public Screen {
    public:
    // Constructor for menu screen.
    // Parameters are items of the menu as strings and their count.
    Menu            (const char ** items, int items_len);
    // Draws the menu screen.
    virtual void    draw();

    private:
    // Choose the selected item.
    // Returns index of the selected item.
    virtual int     middle_press();
    // Scroll up
    // Returns -1
    virtual int     up_press();
    // Scroll down
    // Returns -1
    virtual int     down_press();

    int             select;
    int             select_offset;
    const char **   items;
    int             items_len;
};

// Status board screen to display status of variables.
class StatusBoard : public Screen {
    public:
    StatusBoard();
    // Draws the status screen.
    virtual void    draw();
    // Updates the status values.
    void            update(Status st);

    private:
    // Exits the status screen.
    // Returns -2.
    virtual int     middle_press();

    Status          current_st;
};

// String input screen to provide interface for entering printable ASCII chars.
class StringInput : public Screen {
    public:
    // Default constuctor for string input screen.
    StringInput();
    // Constructor of string input screen.
    // Parameters are:
    //      dialogue - text which should communicate to the user what to type
    //      max_input - max length of the input without terminating zero, will be MAX_STRING_INPUT at most.
    StringInput(const char * dialogue, int max_input);
    // Draws the string input screen - dialogue selection, preview and controls
    virtual void    draw();

    // function to copy the input to buffer
    // returns text length of the input that was copied
    int             copy_text(char * buffer);

    private:
    // Choose selected item
    // Returns:
    // -1 on character
    // -2 on cancel input
    // 0 on confirm input
    virtual int     middle_press();
    // Scroll up
    // Returns -1
    virtual int     up_press();
    // Scroll down
    // Returns -1
    virtual int     down_press();
    // Scroll left
    // Returns -1
    virtual int     left_press();
    // Scroll right
    // Returns -1
    virtual int     right_press();

    int             select_x;
    int             select_y;
    int             select_offset;
    const char *    dialogue;
    int             max_input;
    char            text[MAX_STRING_INPUT + 1];
    int             text_len;
};

// Confirmation screen for yes/no choices.
class Confirmation : public Screen {
    public:
    // Default constructor of confirmation screen.
    Confirmation();
    // Constructor of confirmation screen.
    // Parameter is dialogue - text which should communicate to the user what choice is he making.
    Confirmation(const char * dialogue);

    // Draws confirmation screen - dialogue and choice selection.
    virtual void    draw();

    private:
    // Confirm/cancel
    // Returns 0 on confirmation, -2 on cancel.
    virtual int     middle_press();
    // Scroll left
    // Returns -1
    virtual int     left_press();
    // Scroll right
    // Returns -1
    virtual int     right_press();

    const char *    dialogue;
    bool            confirm;
};



// Time setting screen
class TimeSet : public Screen {
    public:
    // Default constructor for new timeset screen.
    // Default date is 12:00:00 01/01/2000.
    TimeSet();
    // Constructor for new timeset screen.
    // Parameter is initial date to set the screen to.
    TimeSet(DateTime initial);
    // Draws the timesetting screen.
    virtual void    draw();
    // Returns DateTime instance from set variables.
    DateTime        get_datetime();
    private:
    // Provides max day for certain month and year.
    static uint8_t max_day(uint8_t month, uint16_t year);

    virtual int     middle_press();
    // Increment selected
    // Returns -1
    virtual int     up_press();
    // Decrement selected
    // Returns -1
    virtual int     down_press();
    // Scroll left
    // Returns -1
    virtual int     left_press();
    // Scroll right
    // Returns -1
    virtual int     right_press();

    int             offset;
    uint8_t         hours;
    uint8_t         minutes;
    uint8_t         seconds;
    uint8_t         day;
    uint8_t         month;
    uint16_t        year;
};