#pragma once
#include "status.h"

// Output states for ui_loop.
// Represent requests/tasks to be done in the main loop.
#define UI_OUT_DEFAULT      0
#define UI_OUT_WIFI_SSID    1
#define UI_OUT_WIFI_PWD     2
#define UI_OUT_ACC_USR      3
#define UI_OUT_ACC_PWD      4
#define UI_OUT_SET_CONFIRM  5
#define UI_OUT_SET_RTC      6

// Initializes user interface.
// NOTE: Use Wire/I2C begin function before calling this.
void ui_init();

// User interface loop function.
// Calls loop function for buttons.
// Parameters are output buffer for user entered data and output len for length of the data.
// Returns:
// UI_OUT_DEFAULT - no action to take
// UI_OUT_WIFI_SSID - output buffer contains new wifi ssid
// UI_OUT_WIFI_PWD - output buffer contains new wifi password
// UI_OUT_ACC_USR - output buffer contains new website access username
// UI_OUT_ACC_PWD - output buffer contains new website access password
// UI_OUT_ACC_SET_CONFIRM - request to save all changes to wifi/website data to memory
// UI_OUT_SET_RTC - date_out contains new date to set RTC to
int ui_loop(DateTime &date_out, char * output_buffer, int &output_len);

// Updates status for the ui.
void ui_update_status(Status st);