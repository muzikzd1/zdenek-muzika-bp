#pragma once

// Initializes DHT11 temperature sensor.
void dht_init();

// Get measurement of temperature and humidity,
// Parameters are output for measurement.
// If measurement is not succesful, does not modify output parameters.
// Returns true if measurement was successful, false otherwise.
bool dht_reading(float &out_temperature, float &out_humidity); 