#pragma once
#include "RTClib.h"

// Initializes rtc module.
// If time tracking was lost, sets the time to 1/1/2000 by defult.
// NOTE: Use Wire/I2C begin function before calling this.
void        rtc_init();

// Returns current datetime, as is tracked by the RTC.
DateTime    get_datetime();

// Sets the RTC to a new time.
void        set_datetime(DateTime setting);